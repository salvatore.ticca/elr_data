import argparse
from preprocess_data import *
import pickle as pkl

def save_dump(obj,output_path,name):
    if(not os.path.exists(output_path)):
        os.makedirs(output_path)

    pkl.dump(obj,open(os.path.join(output_path,name),"wb"))

def load_dump(path):
    if(os.path.exists(path)):
        obj=pkl.load(open(path,"rb"))
        return obj
    else:
        raise FileNotFoundError('Error, file not found')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Data preprocessing for miniKinetics_200')
    parser.add_argument('traindata_dir', help='Folder in whihc the training dataset is stored')
    parser.add_argument('valdata_dir', help='Folder in which the validation dataset is stored')
    parser.add_argument('output_dir',help='folder where to save the tfrecords')
    parser.add_argument('--resolution',default='LR',help='resolution of the preprocessed dataset (HR/LR)')
    parser.add_argument('--overwrite',default=False,type=bool,nargs='?',const=True,help='if True previously computed tfrecords will be overwritten')
    parser.add_argument('--trainset_partition',default='train_1',help='specify the portion of the data to process')
    args = parser.parse_args()

    if(not os.path.exists(args.traindata_dir)):
        raise FileNotFoundError('Dataset folder not found')

    if(not os.path.exists(args.valdata_dir)):
        raise FileNotFoundError('Validationset folder not found')

    if(not args.resolution in ['HR','LR']):
        raise AttributeError('The specified resolution is not supported. Valid values are HR or LR')

    
    split_file='splits.pkl'

    if(not os.path.exists(os.path.join(args.output_dir,split_file))):

        train_test_splits=get_splits(args.traindata_dir,split_prefix='train',shuffle=True,partitions=4)+get_splits(args.valdata_dir,split_prefix='validation',shuffle=True,partitions=1)
        
        save_dump(train_test_splits,args.output_dir,split_file)
        print([(el[0],len(el[1])) for el in train_test_splits])
        print('splits saved')
    else:
        train_test_splits=load_dump(os.path.join(args.output_dir,split_file))
        print('train/test splits loaded from dump', split_file)

    for el in train_test_splits:
        print(el[0],len(el[1])) 

    lr_size= (32,32) if args.resolution == 'HR' else (16,12)

    print('Start creation of  {} dataset. ({})'.format(args.resolution,lr_size))
    save_dir=os.path.join(args.output_dir,args.resolution)

    create_tfrecords([train_test_splits[i] for i in range(len(train_test_splits)) if train_test_splits[i][0]==args.trainset_partition],
                        output_dir=save_dir,out_size=(32,32),lr_size=lr_size,opt_size=(32,32),
                        record_videos=200,overwrite=args.overwrite)
