import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import cv2
import os
import numpy as np
import time


def get_frames(video_path,out_size,lr_size,interpolation_method,format='rgb',verbose=False):
    cap = cv2.VideoCapture(video_path)
    video=[]

    res=int(cap.get(3)),int(cap.get(4))
    if(verbose):
        print('resizing {} from {} to {} then to {}'.format(video_path,res,lr_size,out_size))

    while True:
        ret,frame=cap.read()

        if(not ret):
            break
        
        if(not out_size is None):
            frame=cv2.resize(frame,lr_size,interpolation=interpolation_method)
            frame=cv2.resize(frame,out_size,interpolation=interpolation_method)

        if(format=='gray'):
            frame=np.expand_dims(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY),2)
        elif(format=='rgb'):
            frame=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        video.append(frame)
        
    return video

def min_max(image):
    mmin = np.min(image)
    mmax = np.max(image)
    return mmin, mmax
    
def normaliseTo01(image):
    mmin, mmax = min_max(image)
    if mmin != 0 or mmax != 1:
       new_min = image - mmin 
       newmax = new_min/np.max(new_min)
    else: newmax = image
    return newmax    

def gen_laplacian_mask(video, opt_size = None, out_size = None ):
    video_res = video[0].shape[1],video[0].shape[0]
    
    if(opt_size is None):
        opt_size=video_res
    
    if(out_size is None):
        out_size=opt_size     
     
    ddepth = cv2.CV_16S    
    num = len(video)
    batch = 3
    nx = 1
    ny = 1
    J = np.arange(0, num, batch)
    """ Faccio un blur dell' immagine """
    videoB = [cv2.GaussianBlur( cv2.cvtColor( frame, cv2.COLOR_RGB2GRAY), (3, 3), 0.4) for frame in video]
    
    V1 = np.array([cv2.Sobel(videoB[j], ddepth, 1, 0, ksize=5) for j in J[0:-1]])
    V2 = np.array([cv2.Sobel(videoB[j], ddepth, 1, 0, ksize=5) for j in J[1:len(J)]])
    M0 = [cv2.GaussianBlur(np.abs( V2[jj,:,:]- V1[jj,:,:] ), (3, 3), 0.4)  for jj in range(V1.shape[0] ) ]
    Rec =  np.array([imm for imm in M0 for i in range(batch)]) 
    frame_to_add = len(video)-len(Rec)
    if frame_to_add == 1:
        maskb = np.concatenate([[Rec[0,:,:]], Rec], axis = 0)
    elif frame_to_add == 2:
        maskb = np.concatenate([[Rec[0,:,:]], Rec, [Rec[-1,:,:]]], axis = 0)
    else:
         maskb = np.concatenate([[Rec[0,:,:]], Rec, [Rec[-1,:,:]], [Rec[-1,:,:]]], axis = 0)
        
    maskb = normaliseTo01(maskb)
    maska =  np.zeros_like(maskb)
    maska[:, nx : video[0].shape[0] - nx, ny: video[0].shape[1] - ny] = maskb[:, nx : video[0].shape[0] - nx, ny: video[0].shape[1] - ny]
   
    kernel_size = 3  
    
    videoC = np.zeros_like(videoB)
    videoB = np.array(videoB)
    videoC[:, nx : video[0].shape[0] - nx, ny: video[0].shape[1] - ny] = videoB[:, nx : video[0].shape[0] - nx, ny: video[0].shape[1] - ny]
    deltaL = np.array([cv2.convertScaleAbs(cv2.Laplacian(filt, ddepth, ksize = kernel_size)) for filt in videoC])
    deltaL = normaliseTo01(deltaL) 

    
    if(np.mean(np.abs(maska))>0.01):
        Mask = deltaL * maska
    else:
        Mask = deltaL
    
    """ Attenzione che mask e' un array  di dimensione (numero_frame, width, height, 1) """
    
    Mask = np.expand_dims(Mask, axis = 3)
    return Mask  

def get_opticalflow(video,interpolation_method=cv2.INTER_CUBIC,opt_size=None,flow_alg='tvl1',out_size=None):
    video_res=video[0].shape[1],video[0].shape[0]
    
    if(opt_size is None):
        opt_size=video_res

    if(out_size is None):
        out_size=video_res

    prev=cv2.resize(cv2.cvtColor(video[0], cv2.COLOR_RGB2GRAY),opt_size,interpolation=interpolation_method)
    flow_stream=[]
    for i in range(1,len(video)):
        current=cv2.resize(cv2.cvtColor(video[i], cv2.COLOR_RGB2GRAY),opt_size,interpolation=interpolation_method)
        if(flow_alg=='tvl1'):
            optical_flow = cv2.DualTVL1OpticalFlow_create()
            flow = optical_flow.calc(prev,current, None)
        elif(flow_alg=='fb'):
            flow = cv2.calcOpticalFlowFarneback(prev,current,None, 0.5, 3, 15, 3, 5, 1.2, 0)
        else:
            raise NotImplementedError('the required optical flow algorithm is not supported')
        
        flow_stream.append(cv2.resize(flow,out_size,interpolation=interpolation_method))
        prev=current.copy()
    
    if(len(flow_stream)>0):
        flow_stream.append(flow_stream[-1].copy())

    return np.array(flow_stream)

def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  if isinstance(value, type(tf.constant(0))):
    value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def _float_feature(value):
  """Returns a float_list from a float / double."""
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def _int64_feature(value):
  """Returns an int64_list from a bool / enum / int / uint."""
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def get_tfexample(frames,action_label,action_id,video_name,flow=None,fg_mask=None):
    encoded_frames=[cv2.imencode(".png", frame)[1].tobytes() for frame in frames]
    features = {
        'frames': _bytes_feature(encoded_frames),#_float_feature(frames.flatten()),
        'video_name':_bytes_feature([video_name.encode()]),
        'action_label':_bytes_feature([action_label.encode()]),
        'action_id': _int64_feature([action_id]),
        'shape':  _int64_feature(list(frames.shape)),
    }

    if(not flow is None):
        features['flow'] = _float_feature(flow.flatten())
        features['flow_shape'] =  _int64_feature(list(flow.shape)),
    
    if(not fg_mask is None):
        features['fg_mask'] = _float_feature(fg_mask.flatten())
        features['fg_mask_shape'] =  _int64_feature(list(fg_mask.shape)),

    return tf.train.Example(features=tf.train.Features(feature=features))

def get_splits(dataset_dir,split_prefix,shuffle=True,partitions=4):
       
    actions=[d for d in os.listdir(dataset_dir) if os.path.isdir(os.path.join(dataset_dir,d))]
    action_idx={a:i for i,a in enumerate(actions)}
    videos=[]
    for a in actions:
        current_dir=os.path.join(dataset_dir,a)
        for video in os.listdir(current_dir):
            v=(os.path.join(current_dir,video),a,action_idx[a])
            videos.append(v)

    if(shuffle):
        np.random.shuffle(videos)
    
    n=int(len(videos)/partitions)
    out=[]
    print('tot videos: ',len(videos))
    for i in range(partitions):
        out.append(('{}_{}'.format(split_prefix,i+1),videos[n*i:n*(i+1)]))
    
    return out

def create_tfrecords(videos,output_dir,out_size,lr_size,interpolation_method=cv2.INTER_CUBIC,flow_alg='tvl1',bg_alg='MOG2',opt_size=None,record_videos=200,overwrite=False,frames_format='rgb'):
    fg_mask=flow=None

    for split in videos:
        print('-'*50)
        print('processing ',split[0],' split')
        
        split_dir=os.path.join(output_dir,split[0])
        if(not os.path.exists(split_dir)):
            os.makedirs(split_dir)
        
        for idx in range(0, len(split[1]),record_videos):
            print('--'*10)
            
            tfrecord_file = os.path.join(split_dir,'block_{}_{}.tfrecords'.format(idx,min(idx+record_videos,len(split[1]))))
            if(os.path.exists(tfrecord_file) and overwrite == False):
                print('{} already exists, skipping'.format(tfrecord_file))
                continue
            
            print('processing videos {} to {}'.format(idx,idx+record_videos))
            
            with tf.io.TFRecordWriter(tfrecord_file) as writer:
                for v,action,action_idx in split[1][idx:idx+record_videos]:
                    s=time.time()
                    frames=get_frames(v,out_size=out_size,lr_size=lr_size,interpolation_method=interpolation_method,format=frames_format)  
                    frames=np.array(frames)
                    
                    s=time.time()
                    if(not bg_alg is None):
                        fg_mask=gen_laplacian_mask(np.uint8(frames.copy()))
                   
                    if(not flow_alg is None):
                        flow=get_opticalflow(frames,
                                             interpolation_method=cv2.INTER_CUBIC,
                                             out_size=out_size,
                                             flow_alg=flow_alg,
                                             opt_size=opt_size)
                    
                    tfexample=get_tfexample(frames,action,action_idx,v,flow,fg_mask).SerializeToString()    
                    print(v,time.time()-s)
                    print(frames.shape,flow.shape,fg_mask[0].shape,frames.dtype,flow.dtype)
                    writer.write(tfexample)

    print('operation completed, tfrecord files saved in ',output_dir)


